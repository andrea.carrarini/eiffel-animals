note
	description: "Summary description for {GRASS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GRASS

inherit

	FOOD
		redefine
			out
		end

feature

	out: STRING
		do
			Result := "a bunch of grass (" + weight.out + "kg)"
		end

	consume (q: INTEGER)
		do
			weight := weight - q
		end

	grow (q: INTEGER)
		do
			weight := weight + q
		end

	weight: INTEGER

end
